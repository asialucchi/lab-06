package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends IllegalStateException {
	
	private final double balance;
	private final double amount;
	
	public NotEnoughFoundsException (final double balance, final double amount) {
		this.balance = balance;
		this.amount = amount;
	}
}
