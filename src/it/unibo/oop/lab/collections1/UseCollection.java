package it.unibo.oop.lab.collections1;

import java.util.*;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {
	
	private final static int FIRST_NUMBER = 1000;
	private final static int LAST_NUMBER = 2000;
	private final static int NEW_ELEMS = 1000000;
	private final static int READ_ELEMS = 1000;
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	final ArrayList<Integer> numbersArray = new ArrayList<>();
    	for (int i=FIRST_NUMBER; i<LAST_NUMBER; i++) {
    		numbersArray.add(i);
    	}
    	
    	final LinkedList<Integer> numbersList = new LinkedList<>(numbersArray);
    	
    	int temp = numbersList.removeLast();
    	numbersList.addLast(numbersList.removeFirst());
    	numbersList.addFirst(temp);
    	
    	for (int num: numbersArray) {
    		System.out.print(num + ", ");
    	}
    	System.out.println();
    	System.out.println();
    	
    	long arrayTime = System.nanoTime();
        for (int i = 1; i <= NEW_ELEMS; i++) {
            numbersArray.add(i);
        }
        arrayTime = System.nanoTime() - arrayTime;
        System.out.println("Timo to add " + NEW_ELEMS + " numbers to the array " + arrayTime);
        
        long listTime = System.nanoTime();
        for (int i = 1; i <= NEW_ELEMS; i++) {
            numbersList.add(i);
        }
        listTime = System.nanoTime() - listTime;
        System.out.println("Timo to add " + NEW_ELEMS + " numbers to the list " + listTime);
        System.out.println();
        
        //6
        
        long arrayTimeReadElems = System.nanoTime();
        for (int i = 0; i < READ_ELEMS; i++) {
        	numbersArray.get(i);
        }
        arrayTimeReadElems = System.nanoTime() - arrayTimeReadElems;
        long arrayTimeMidElem = System.nanoTime();
        numbersArray.get(NEW_ELEMS/2);
        arrayTimeMidElem = System.nanoTime() - arrayTimeMidElem;
        System.out.println("Time to read " + READ_ELEMS + " from the array is " + arrayTimeReadElems + " whereas the time to read the middle elem is " + arrayTimeMidElem);
        
        long listTimeReadElems = System.nanoTime();
        for (int i = 0; i < READ_ELEMS; i++) {
        	numbersList.get(i);
        }
        listTimeReadElems = System.nanoTime() - listTimeReadElems;
        long listTimeMidElem = System.nanoTime();
        numbersList.get(NEW_ELEMS/2);
        listTimeMidElem = System.nanoTime() - listTimeMidElem;
        System.out.println("Time to read " + READ_ELEMS + " from the list is " + listTimeReadElems + " whereas the time to read the middle elem is " + listTimeMidElem);
        
        final Map<String, Long> map = new HashMap<>();
        map.put("Africa", 1110635000L);
        map.put("Americas", 972005000L);
        map.put("Antartica", 0L);
        map.put("Asia", 4298723000L);
        map.put("Europe", 742452000L);
        map.put("Oceania", 38304000L);
        
        long sum = 0;
        for (long elem: map.values()) {
        	sum += elem;
        }
        System.out.println();
        System.out.println("Total Population: " + sum);
    }
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
}
