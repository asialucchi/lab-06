package it.unibo.oop.lab06.generics1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class GraphImpl<N> implements Graph<N>{
	private Map <N, Collection<N>> graph = new HashMap <N, Collection<N>>();

	@Override
	public void addNode(N node) {
		graph.put(node, new HashSet<N>());
	}

	@Override
	public void addEdge(N source, N target) {
		graph.get(source).add(target);
	}

	@Override
	public Set nodeSet() {
		return graph.keySet();
	}

	@Override
	public Set linkedNodes(N node) {
		return (Set) graph.get(node);
	}

	@Override
	public List getPath(N source, N target) {
		Queue<N> coda = new LinkedList<N>();
		Map<N,N> reverseLinks = new HashMap<N,N>();
		N actualNode;
		
		coda.add(source);
		while (coda.isEmpty() == false) {
			actualNode=coda.remove();
			for (N elem: graph.get(actualNode)) {
				if (!reverseLinks.keySet().contains(elem)) {
					coda.add(elem);
					reverseLinks.put (elem, actualNode);
				}
			}
		}
		
		LinkedList<N> path = new LinkedList<N>();
		actualNode = target;
		while(actualNode != source) {
			actualNode = reverseLinks.get(actualNode);
			path.addFirst(actualNode);
		}
		path.removeFirst();
		
		return path;
	}
}
